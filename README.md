# DFT Tools
A few tools I developed to analyse DFT calculations results useing elk.

Currently this repo contains the following tools:
1. Broadening.jl - A tool written in [Julia](https://julialang.org) to broaden the DFT Spectra using Gaussian and Lorentzian functions.
2. SpeciesSum    - A python based script to add the PDOS for a particular species.
3. Integrator    - Integrates the generated valence band spectrum to obtain the electron count.
