#!/usr/bin/env julia
# -*- encoding: utf-8 -*-
#
# Broadening - Simple script to broaden DFT DOS
# Version 0.1.0
#
# Copyright 2022 Britanicus <marcusbritanicus@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# ( at your option ) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA 02110-1301, USA.

using DelimitedFiles

# Read the data, and return the list of x values, y values, and step size
function readData( fn )
    data = split( strip( read( fn, String ) ), "\n" )

    xlist = Vector{Float64}()
    ylist = Vector{Float64}()
    for line in data
	if line[ 1 ] == "#"
		continue
	end

        parts = split( strip( line ) )
        try
            x = parse( Float64, parts[ 1 ] )
            y = parse( Float64, parts[ 2 ] )

            push!( xlist, x )
            push!( ylist, y )

        catch y
            print( "Bad data:", line )

        end
    end

    return ( xlist, ylist, abs( xlist[ 1 ] - xlist[ 2 ] ) )
end

# 300 meV FWHM
function Gaussian( x, x0, fwhm = 0.3 )
    sd = fwhm / ( 2.0 * sqrt( 2.0 * log( 2.0 ) ) )

    return ( 1.0 / ( sd * sqrt( 2.0 * pi ) ) ) * exp( -( ( x - x0 ) ^ 2.0 ) / ( 2.0 * sd ^ 2.0 ) )
end

function Lorentzian( x, x0, dw = 0.1 )
    w = 0.2 + dw * abs( x0 )

    return ( 2.0 / pi ) * ( w / ( 4 * ( x - x0 ) ^ 2.0 + w ^ 2.0 ) )
end

function getFileName()
    for arg in ARGS[ 1: end]
        if !startswith( arg, "--" )
            return arg
        end
    end

    print( "Name of the input file: " )
    return readline()
end

function getGaussianFWHM()
    for arg in ARGS[ 1: end]
        if startswith( arg, "--gauss_fwhm=" )
            return parse( Float64, replace( arg, "--gauss_fwhm=" => "" ) )
        end
    end

    return 0.3

end

function getLorentzianFWHM()
    for arg in ARGS[ 1: end]
        if startswith( arg, "--lorentz_fwhm=" )
            return parse( Float64, replace( arg, "--lorentz_fwhm=" => "" ) )
        end
    end

    return 0.3
end

function ConvoluteWithGaussian( X, Y, G_FWHM, Step )
    println( "Broadening with Gaussian: FWHM = $G_FWHM, Step = $Step, Points = $(length(X)) " )

    Gauss = zeros( Float64, length( X ) )
    for i = 1: length( X )
        sum = 0

        lc = 0
        for j = 1: length( X )
            sum += abs( Gaussian( X[ i ], X[ j ], G_FWHM ) * Y[ j ] * Step )
        end
        Gauss[ i ] = sum
    end

    return Gauss
end

function ConvoluteWithLorentzian( X, Y, L_FWHM, Step )
    println( "Broadening with Lorentzian: FWHM = $L_FWHM, Step = $Step, Points = $(length(X)) " )

    Lorentz = zeros( Float64, length( X ) )
    for i = 1:length( X )
        sum = 0
        for j = 1:length( X )
            sum += abs( Lorentzian( X[ i ], X[ j ], L_FWHM ) * Y[ j ] * Step )

        end
        Lorentz[ i ] = sum
    end

    return Lorentz
end

function writeOutput( X, Y, F, fn )
    f = open( fn, "w" )

    writedlm( f, [ X Y F ] )

    close( f )
end

function Convolute()
    filename = getFileName()
    println( "Input file name: ", filename )
    X, Y, Step = readData( filename )

    G_FWHM = getGaussianFWHM()
    L_FWHM = getLorentzianFWHM()

    Final = []

    if ( "--convolute=G" in ARGS )
        global Final = ConvoluteWithGaussian( X, Y, G_FWHM, Step )

    elseif ( "--convolute=L" in ARGS )
        global Final = ConvoluteWithLorentzian( X, Y, L_FWHM, Step )

    elseif ( "--convolute=GL" in ARGS )
        Temp = ConvoluteWithGaussian( X, Y, G_FWHM, Step )
        global Final = ConvoluteWithLorentzian( X, Temp, L_FWHM, Step )

    elseif ( "--convolute=LG" in ARGS )
        Temp = ConvoluteWithLorentzian( X, Y, L_FWHM, Step )
        global Final = ConvoluteWithGaussian( X, Temp, G_FWHM, Step )

    else
        Temp = ConvoluteWithGaussian( X, Y, G_FWHM, Step )
        global Final = ConvoluteWithLorentzian( X, Temp, L_FWHM, Step )
    end

    tmp = tempname()
    writeOutput( X, Y, Final, tmp )

    if "--no-replace" in ARGS
        mv( tmp, "Broadened_" * filename, force = true )

    else
        mv( tmp, filename, force = true )

    end
end


Convolute()
